package geromercante.marvelchallenge.services;

import java.util.List;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.ComicDto;

public interface ComicService {

  List<ComicDto> findAll(Pageable pageable, Long characterId);

  ComicDto findById(Long comicId);
}
