package geromercante.marvelchallenge.services;

import java.util.List;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.CharacterDto;

public interface CharacterService {
  
  List<CharacterDto> findAll(Pageable pageable, String name, int[] comics, int[] series);

  CharacterDto.CharacterInfoDto findInfoById(Long characterId);
}
