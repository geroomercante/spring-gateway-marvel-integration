package geromercante.marvelchallenge.services;

import java.util.Map;

// Interface generica
public interface HttpClientService {

  <T> T doGet(String endpoint, Map<String, String> queryParams, Class<T> reponseType);

  <T, R> T doPost(String endpoint, Map<String, String> queryParams, Class<T> reponseType, R bodyRequest);

  <T, R> T doPut(String endpoint, Map<String, String> queryParams, Class<T> reponseType, R bodyRequest);

  <T> T doDelete(String endpoint, Map<String, String> queryParams, Class<T> reponseType);

}
