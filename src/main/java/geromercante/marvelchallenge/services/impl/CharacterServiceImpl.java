package geromercante.marvelchallenge.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.CharacterDto;
import geromercante.marvelchallenge.persistence.integration.marvel.repository.CharacterRepository;
import geromercante.marvelchallenge.services.CharacterService;

@Service
public class CharacterServiceImpl implements CharacterService {

  @Autowired
  private CharacterRepository characterRepository;

  @Override
  public List<CharacterDto> findAll(Pageable pageable, String name, int[] comics, int[] series) {
    return characterRepository.findAll(pageable, name, comics, series);
  }

  @Override
  public CharacterDto.CharacterInfoDto findInfoById(Long characterId) {
    return characterRepository.findInfoById(characterId);
  }

}
