package geromercante.marvelchallenge.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.ComicDto;
import geromercante.marvelchallenge.persistence.integration.marvel.repository.ComicRepository;
import geromercante.marvelchallenge.services.ComicService;

@Service
public class ComicServiceImpl implements ComicService {
  
  @Autowired
  private ComicRepository comicRepository;

  @Override
  public List<ComicDto> findAll(Pageable pageable, Long characterId) {
    return comicRepository.findAll(pageable, characterId);
  }

  @Override
  public ComicDto findById(Long comicId) {
    return comicRepository.findById(comicId);
  }
}
