package geromercante.marvelchallenge.persistence.integration.marvel.mapper;

import com.fasterxml.jackson.databind.JsonNode;

import geromercante.marvelchallenge.persistence.integration.marvel.dto.ThumbnailDto;

public class ThumbnailMapper {
  
  public static ThumbnailDto toDto (JsonNode thumbnailNode) {
    if (thumbnailNode == null) {
      throw new IllegalArgumentException("El nodo JSON no puede ser nulo");
    }

    ThumbnailDto thumbnailDto = new ThumbnailDto(
      thumbnailNode.get("path").asText(),
      thumbnailNode.get("extension").asText()
    );

    return thumbnailDto;
  }
}
