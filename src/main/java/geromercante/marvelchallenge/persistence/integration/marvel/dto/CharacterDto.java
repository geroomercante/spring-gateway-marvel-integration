package geromercante.marvelchallenge.persistence.integration.marvel.dto;

public record CharacterDto (
  Long id,
  String name,
  String description,
  String modified,
  String resourceUri
) {

  public static record CharacterInfoDto(
    String imagePath,
    String description
  ) {
    
  }
}
