package geromercante.marvelchallenge.persistence.integration.marvel.repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.MarvelAPIConfig;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.CharacterDto;
import geromercante.marvelchallenge.persistence.integration.marvel.mapper.CharacterMapper;
import geromercante.marvelchallenge.services.HttpClientService;
import jakarta.annotation.PostConstruct;

@Repository
public class CharacterRepository {

  @Autowired
  private MarvelAPIConfig marvelAPIConfig;

  @Autowired
  private HttpClientService httpClientService;

  @Value("${integration.marvel.base-path}")
  private String basePath;

  private String characterPath;

  @PostConstruct
  private void setPath() {
    characterPath = basePath.concat("/").concat("characters");
  }

  public List<CharacterDto> findAll(Pageable pageable, String name, int[] comics, int[] series) {
    Map<String, String> marvelQueryParams = getQueryParamsForFindAll(pageable, name, comics, series);
    JsonNode response = httpClientService.doGet(characterPath, marvelQueryParams, JsonNode.class);
    return CharacterMapper.toDtoList(response);
  }

  public CharacterDto.CharacterInfoDto findInfoById(Long characterId) {
    Map<String, String> marvelQueryParams = marvelAPIConfig.getAuthenticationQueryParams();
    String finalUrl = characterPath.concat("/").concat(Long.toString(characterId));
    JsonNode response = httpClientService.doGet(finalUrl, marvelQueryParams, JsonNode.class);
    return CharacterMapper.toInfoDtoList(response).get(0);
  }

  private Map<String, String> getQueryParamsForFindAll(Pageable pageable, String name, int[] comics, int[] series) {
    Map<String, String> marvelQueryParams = marvelAPIConfig.getAuthenticationQueryParams();
    marvelQueryParams.put("offset", Long.toString(pageable.offset()));
    marvelQueryParams.put("limit", Long.toString(pageable.limit()));
    if(StringUtils.hasText(name)) {
      marvelQueryParams.put("name", name);
    }
    if(comics != null) {
      String comicsAsString = this.joinIntArray(comics);
      marvelQueryParams.put("comics", comicsAsString);
    }
    if(series != null) {
      String seriesAsString = this.joinIntArray(series);
      marvelQueryParams.put("series", seriesAsString);
    }
    return marvelQueryParams;
  }


  private String joinIntArray(int[] comics) {
    List<String> stringArray = IntStream.of(comics).boxed()
      .map(each -> each.toString())
      .collect(Collectors.toList());
    return String.join(", ", stringArray);
  }

}
