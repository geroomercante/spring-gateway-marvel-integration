package geromercante.marvelchallenge.persistence.integration.marvel.dto;

public record ThumbnailDto(
  String path,
  String extension
) {
  
}
