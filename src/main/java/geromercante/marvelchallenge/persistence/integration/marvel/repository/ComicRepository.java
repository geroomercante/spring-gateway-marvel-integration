package geromercante.marvelchallenge.persistence.integration.marvel.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.MarvelAPIConfig;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.ComicDto;
import geromercante.marvelchallenge.persistence.integration.marvel.mapper.ComicMapper;
import geromercante.marvelchallenge.services.HttpClientService;
import jakarta.annotation.PostConstruct;

@Repository
public class ComicRepository {

  @Autowired
  private MarvelAPIConfig marvelAPIConfig;

  @Autowired
  private HttpClientService httpClientService;

  @Value("${integration.marvel.base-path}")
  private String basePath;

  private String comicPath;

  @PostConstruct
  private void setPath() {
    comicPath = basePath.concat("/").concat("comics");
  }


  public List<ComicDto> findAll(Pageable pageable, Long characterId) {
    Map<String, String> marvelQueryParams = getQueryParamsForFindAll(pageable, characterId);
    JsonNode reponse = httpClientService.doGet(comicPath, marvelQueryParams, JsonNode.class);
    return ComicMapper.toDtoList(reponse);
  }


  private Map<String, String> getQueryParamsForFindAll(Pageable pageable, Long characterId) {
    Map<String, String> marvelQueryParams = marvelAPIConfig.getAuthenticationQueryParams();
    marvelQueryParams.put("offset", Long.toString(pageable.offset()));
    marvelQueryParams.put("limit", Long.toString(pageable.limit()));
    if(characterId != null && characterId.longValue() > 0) {
      marvelQueryParams.put("characters", Long.toString(characterId));
    }
    return marvelQueryParams;
  }


  public ComicDto findById(Long comicId) {
    Map<String, String> marvelQueryParams = marvelAPIConfig.getAuthenticationQueryParams();
    String finalUrl = comicPath.concat("/").concat(Long.toString(comicId));
    JsonNode response = httpClientService.doGet(finalUrl, marvelQueryParams, JsonNode.class);
    return ComicMapper.toDtoList(response).get(0);
  }
  
}
