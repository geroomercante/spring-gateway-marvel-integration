package geromercante.marvelchallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import geromercante.marvelchallenge.dto.Pageable;
import geromercante.marvelchallenge.persistence.integration.marvel.dto.ComicDto;
import geromercante.marvelchallenge.services.ComicService;

@RestController
@RequestMapping("/comics")
public class ComicController {

  @Autowired
  private ComicService comicService;

  @GetMapping
  public ResponseEntity<List<ComicDto>> findAll(
    @RequestParam(required = false) Long characterId,
    @RequestParam(defaultValue = "0") long offset,
    @RequestParam(defaultValue = "10") long limit
  ) {
    Pageable pageable = new Pageable(offset, limit);
    return ResponseEntity.ok(comicService.findAll(pageable, characterId));
  }

  @GetMapping("/{comicId}")
  public ResponseEntity<ComicDto> findById(
    @PathVariable Long comicId
  ) {
    return ResponseEntity.ok(comicService.findById(comicId));
  }
}