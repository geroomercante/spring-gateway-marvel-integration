package geromercante.marvelchallenge.dto;

public record Pageable(
  long offset,
  long limit
) {}
